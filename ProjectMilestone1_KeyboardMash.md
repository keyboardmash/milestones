# Milestone 1 - Project Proposal #

### Team Name ###
Keyboard Mash

### Members ###
Joshua Franklin, Megan Felsch, Hongyi Chen, Kyle Neubarth, Derek Ly

### Description ###
Tell a story in the format of Card Against Humanity where there is a starting phrase, defined by a premade starting card, and then a judge player who chooses the theme of the of the next phrase for other players to input the phrase. Themes would be, “Character encounters something” or “Character goes somewhere.” There would be a rare “___ THE END” card which ends the game when chosen by the judge. As the game continues, the chance of drawing this card increases. 

### Vision Statement ###
To provide countless hours of entertainment through an online game designed for social interaction and expressing creativity.

### Motivation ###
We had previous experience playing “Choose Your Own Adventure” type games, and wanted to see what would happen if we provided a collaborative platform for creating a “Choose Your Own Adventure” type story. This online game and social experiment can show us how creative people are working together, but under pressure.

### Risks ###
* Poor communication amongst teammates.
* Not everyone has web development experience, which is what our application will primarily involve.

### Risk Mitigation Plan ###
* We have set up online communication through Discord, and we will all try to reply to each other as soon as possible.
* We will try our best to share knowledge with each other so that we are all familiar with the development platforms we choose before starting.

### Version Control ###
* Frontend:     https://bitbucket.org/keyboardmash/web-app/
* Milestones:   https://bitbucket.org/keyboardmash/milestones/
* Meeting Logs: https://bitbucket.org/keyboardmash/meeting-logs/

### Development Method ###
We will be practicing the Agile development method. To do this will we use Trello which integrates with BitBucket and allows us to link stories with branches. On each of our weekly meetings, we will start off with a stand-up like meeting where we will go around the circle and all say what we worked on last week, what we will work on this week, and if anything is blocking us. Trello will help us keep track of what tasks need to be accomplished, and it will tell us who is working on the issue and how much progress has been made so far.

### Collaboration Tool ###
Atlassian BitBucket, Trello, Google Drive, Discord

### Proposed Architecture ###
We we be creating a web app using HTML, CSS, and JavaScript. Our backend will either utilize Google’s Firebase or some implementation of Web Sockets (either Socket-IO with Python or Node.JS or Django with Django-Channels).

