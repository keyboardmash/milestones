# Milestone #2 - Project Tools & Agile Methodology

### Project Management Tool
We will use [Trello](https://trello.com/).

### Backlog
+ Project planning
    + Finalize the idea
    + Figure out who does what
+ User Interface
    + Design of page
+ Game Design
    + Player roles (Judge or not judge)
    + Rotation of Judge role
    + Random starting phrases
    + Recording of all phrases (starting phrase, winning phrases)
    + Point system?
+ Front End Controller
	+ State controller
+ Backend Controller
    + State controller
+ Networking
    + Design communication protocol between client and server
    + Make sure client and server are on the same state of the game
    + Handle authentication
+ Database Integration
	+ All game are written to database
	+ Cards are stored in database

### Team Focuses
*This is not what we are restricting ourselves to do, this is just what we will focus on.*

**Josh** Networking, Backend Controller

**Kyle** Front End Controller, User Interface

**Hongyi**: User Interface

**Megan**: Game Design, Front End Controller

**Derek**: Database Integration, Networking

### Plan Cycle Within the Project Management Method
+ Sprint #1 (10/13 - 10/20):
    + Finalize flow chart.
+ Sprint #2 (10/20 - 10/27):
    + Invite code generation on game creation.
    + Physical design of front end.
+ Sprint #3 (10/27 - 11/3):
    + Start backend state controller.
    + Start frontend state controller.
+ Sprint #4 (11/3 - 11/10):
    + Connect everything through flask/socket-io.
+ Sprint #5 (11/10 - 11/17):
    + Database integration.
+ Sprint #6 (11/17 - 12/1):
    + Finish the project.
+ Sprint #7 (12/1 - 12/8):
    + Testing/Bug fixes.

### Agile Standup/Retrospective
We will all go around and answer the three questions:
+ What have you completed since the last meeting?
+ What will you complete before the next meeting?
+ Describe any obstacles or roadblocks you face.

From there we will discuss the state of our project, and how we will adjust our approach to accommodate for other teammates and their roadblocks.
